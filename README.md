# AI File Info Process Page
AI File Info Process Page allows users to upload files and obtain their file name,
tilte, format and file size using Python's built-in tools, and extract keywords using AI-enable tool-Gensim.

This tool has been deployed for the Ministry of Government and Consumer Services. It is currently being hosted on [142.144.9.1:3000](142.144.9.1:3000)

# Supported File Format
* .png
* .jpg
* .doc
* .docx
* .pptx
* .pdf
* .mp4
* .wav

# How to Run the App on Your Laptop
**`All required libraries and software are downloaded and installed on a Red Hat Enterprise Linux Server (RHEL in short)`**

### Create a fileInfoGetter environment

#### If your operating system is RHEL

Run the following command in your terminal to clone the project from Gitlab repo:

    $ git clone https://gitlab.com/Cheng1996/ai-file-info-process-page.git
    

#### If your operating system is `NOT` RHEL

##### Option 1:

[Create a virtural machine](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Virtualization/3.0/html/Evaluation_Guide/Evaluation_Guide-SP-Create_New_VM.html), and then run the following command in your terminal to clone the project from Gitlab repo:

    $ git clone https://gitlab.com/Cheng1996/ai-file-info-process-page.git

##### Option 2:

Download and install [all the required libraries and software](#python-libraries-and-software-that-you-will-need) by yourself.

### Activate the environment (skip if you are following option 2)

Run the following command in your terminal

    $ source $HOME/fileInfoGetter/fileInfoGetterEnv/bin/activate

Your terminal should now look like this:

![User-entered text](/READMEimages/virtual_environment.jpg)

### Run the fileInfoGetter file

Finally, we can run the python file in terminal, or direcly run it on any Python IDE if you are following option 2.
    
    $ python3.6 fileInfoGetter.py
    
Enter the following URL in Google Chrome.

    http://localhost:5000

# What You Should See
Home Page
![User-entered text](/READMEimages/Homepage.PNG)

# Python Libraries and Software That You Will Need
All required libraries and software are already installed in virtual environment - fileInfoGetterEnv. Links are given for references.
### Python Libraries:
* [flask](http://flask.pocoo.org/) (to create the web application)
* [flask_dropzone](https://flask-dropzone.readthedocs.io/en/latest/) (to upload files on the website)
* [openpyxl](https://openpyxl.readthedocs.io/en/stable/) (to generate result spreadsheet)
* [gensim](https://radimrehurek.com/gensim/summarization/keywords.html) (to extract keywords given plain text content)
* [docx2txt](https://github.com/ankushshah89/python-docx2txt) (to extract text content from .docx files)
* [pdfminer.six](https://github.com/pdfminer/pdfminer.six) (to extract text content from .pdf files)
* [speech_recognition](https://github.com/Uberi/speech_recognition#readme)(to perform text recognition on .mp4 files)
* [pptx](https://python-pptx.readthedocs.io/en/latest/) (to extract text content from .pptx files)
* [keras](https://keras.io/) (to perform object recognition for image files)
    * We are using the pre-trained model - [NASNet](https://keras.io/applications/#nasnet) to perform this task
* [wand](http://docs.wand-py.org/en/0.5.1/#user-s-guide) (to convert pdf files to png files)

### Software/Tools:
* [antiword](http://www.winfield.demon.nl/) (convert .dco to .docx files)
* [ffmpeg](https://www.ffmpeg.org/) (to convert .mp4 files to .wav files)
* [tesseract](https://github.com/tesseract-ocr/tesseract/wiki#centos-fedora-scientificlinux-opensuse-rhel-packages) (to perform character recognition for image files)
* [ImageMagick](https://www.imagemagick.org/) (to convert bitmap images)
* [TensorFlow](https://www.tensorflow.org/)(to execute Keras)

# How to Deploy Application to a WSGI Server
The application is currently hosting on a Red Hat Enterprise Linux Server (RHEL in short), 
RHEL works almost the same as CentOS, and here is the instruction:
[How To Serve Flask Applications with Gunicorn and Nginx on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7)